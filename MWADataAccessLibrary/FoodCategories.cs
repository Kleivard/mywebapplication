﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MWADataAccessLibrary
{
    public enum FoodCategory
    {
        DINNER,
        BREAKFAST,
        LUNCH,
        SNACK,
        DESERT
    }

    public enum IngredientCategory
    {
        MILK,
        FATS,
        OILS,
        FRUITS,
        GRAINS,
        NUTS,
        HERBS,
        MEAT,
        FISH,
        PASTA,
        RICE,
        VEGETABLES,
        OTHER
    }
}
