﻿using Microsoft.EntityFrameworkCore;
using MWADataAccessLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MWADataAccessLibrary
{
    public class FoodRecipeDbContext : DbContext
    {
        public FoodRecipeDbContext(DbContextOptions options) : base(options){}

        public DbSet<Models.FoodRecipe> FoodRecipes { set; get; }

        public DbSet<Ingredient> Ingredients { set; get; }


        // TODO: Look at the video on how he handeled this and the migration.
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("Data Source=N-SE-01-8481\\SQLEXPRESS;InitialCatalog=FoodRecipeDB;Integrated Security=True;");

        //}
    }
}
