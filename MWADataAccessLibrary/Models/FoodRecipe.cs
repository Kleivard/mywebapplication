﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MWADataAccessLibrary.Models
{

    // TODO: if Ingredient is updated after creating recipe it need to be reevaluated. 
    public class FoodRecipe
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public int Duration { get; set; }

        public int Portions { get; set; }
        // TODO: any annotations if they are generated from code ? Annotation on the source i guess ?
        public int? TotalKcal { get; set; }
        public int? TotalCarbohydrates { get; set; }
        public int? TotalProtein { get; set; }
        public int? TotalFat { get; set; }
        public int? TotalSugar { get; set; }
        public int? TotalFiber { get; set; }

        public int? PortionKcal { get; set; }
        public int? PortionCarbohydrates { get; set; }
        public int? PortionProtein { get; set; }
        public int? PortionFat { get; set; }
        public int? PortionSugar { get; set; }
        public int? PortionFiber { get; set; }

        [Required] // Have this value as a dropdown
        public FoodCategory FoodCategory { get; set; }

        [Required]
        public List<Ingredient> Ingredients { get; set; }

        [Required]
        [MaxLength(2000)]
        public string Description { get; set; }
        // TODO: Figure out intruction handling.
        public string Instructions { get; set; }

        // For pic install storageClient. Use blob to save picture...

    }
}
