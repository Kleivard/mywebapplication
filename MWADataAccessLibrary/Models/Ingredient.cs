﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MWADataAccessLibrary.Models
{
    public class Ingredient
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        [Required]
        public float Kcal { get; set; }
        [Required]
        public float Carbohydrates { get; set; }
        [Required]
        public float Protein { get; set; }
        [Required]
        public float Fat { get; set; }

        public float Sugar { get; set; }

        public float Fiber { get; set; }
        [Required] // Forced by dropdown menu
        public IngredientCategory Category { get; set; }
        [MaxLength(1000)]
        public string Description { set; get; }

        public int amount { get; set; }

        // Add enum for dl, ml
    }
}
